<?php
declare(strict_types=1);
namespace F2\PDO;

use F2;
use F2\Container\AbstractServiceProvider;
use PDO;

F2::container()->addServiceProvider(new class extends AbstractServiceProvider {
    protected $provides = [
        'PDO',
    ];

    public function register() {
        $this->getContainer()
            ->add('PDO', function() {
                $url = F2::env("DATABASE_URL");
                if (!$url) {
                    throw new Exception("Environment variable DATABASE_URL is not defined");
                }
                $url = str_replace(":///", "://localhost/", $url);
                $p = parse_url($url);
                if (!empty($p["path"])) {
                    $p["path"] = substr($p["path"], 1);
                }
                switch ($p["scheme"]) {
                    case "sqlite":
                    case "sqlite3":
                    case "pdo-sqlite":
                        $pdo = new PDO("sqlite:".$p["path"], null, null, [
                            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                            PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
                        ]);
                        break;
                    case "mysql":
                    case "mysql2":
                    case "pdo-mysql":
                        $pdo = new PDO("mysql:host=".$p["host"].";dbname=".$p["path"].(!empty($p["port"]) ? ";port=".$p["port"] : ""), $p["user"] ?? null, $p["pass"] ?? null, [
                            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                            PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
                        ]);
                        break;
                    case "pgsql":
                    case "postgres":
                    case "postgresql":
                    case "pdo-pgsql":
                        $pdo = new PDO("pgsql:host=".$p["host"].";dbname=".$p["path"].";charset=utf8".(!empty($p["port"]) ? ";port=".$p["port"] : ""), $p["user"] ?? null, $p["pass"] ?? null, [
                            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                            PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
                        ]);
                        break;
                    case "oci8":
                    case "pdo-oci":
                        $pdo = new PDO("oci:dbname=//".$p["host"].(!empty($p["port"]) ? ":".$p["port"] : "" )."/".$p["path"].";charset=UTF8".(!empty($p["port"]) ? ";port=".$p["port"] : ""), $p["user"] ?? null, $p["pass"] ?? null, [
                            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                            PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
                        ]);
                        break;
                    case "sqlsrv":
                    case "pdo-sqlsrv":
                        $pdo = new PDO("sqlsrv:Server=".$p["host"].(!empty($p["port"]) ? ",".$p["port"] : "" ).";Database=".$p["path"].";charset=UTF8".(!empty($p["port"]) ? ";port=".$p["port"] : ""), $p["user"] ?? null, $p["pass"] ?? null, [
                            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                            PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
                        ]);
                        break;
                    default:
                        throw new Exception("Unsupported driver '".$p["scheme"]."'. Consider using f2/pdo-from-doctrine-dbal.");
                }
                return $pdo;
            });
    }
});
